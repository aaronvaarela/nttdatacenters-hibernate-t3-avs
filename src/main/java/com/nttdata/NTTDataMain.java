package com.nttdata;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.nttdata.persistence.Cliente;
import com.nttdata.persistence.Contrato;
import com.nttdata.services.ClienteServiceImpl;
import com.nttdata.services.ContratoServiceImpl;

/**
 * Clase principal
 * 
 * @author Aaron
 *
 */
@SpringBootApplication
public class NTTDataMain implements CommandLineRunner {

	@Autowired
	private ClienteServiceImpl clienteService;
	
	@Autowired
	private ContratoServiceImpl contratoService;
	
	/**
	 * Método principal
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		SpringApplication.run(NTTDataMain.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		
		// Crear clientes y contratos, y almacenarlos en la BD
		Cliente cliente1 = new Cliente();
		cliente1.setDocumentoIdentidad("11111111A");
		cliente1.setNombre("Paco");
		cliente1.setPrimerApellido("García");
		cliente1.setSegundoApellido("Cruz");
		
		Cliente cliente2 = new Cliente();
		cliente2.setDocumentoIdentidad("22222222B");
		cliente2.setNombre("María");
		cliente2.setPrimerApellido("Fernández");
		cliente2.setSegundoApellido("sanchez");
		
		clienteService.agregarCliente(cliente1);
		clienteService.agregarCliente(cliente2);
		
		cliente2.setSegundoApellido("Sánchez");
		clienteService.actualizarCliente(cliente2);
		
		Contrato contrato1 = new Contrato();
		contrato1.setCliente(cliente1);
		contrato1.setFechaVigencia(new Date());
		contrato1.setFechaCaducidad(new Date());
		contrato1.setPrecioMensual(12.0d);
		
		Contrato contrato2 = new Contrato();
		contrato2.setCliente(cliente1);
		contrato2.setFechaVigencia(new Date());
		contrato2.setFechaCaducidad(new Date());
		contrato2.setPrecioMensual(28.0d);
		
		Contrato contrato3 = new Contrato();
		contrato3.setCliente(cliente2);
		contrato3.setFechaVigencia(new Date());
		contrato3.setFechaCaducidad(new Date());
		contrato3.setPrecioMensual(86.0d);
		
		contratoService.insert(contrato1);
		contratoService.insert(contrato2);
		contratoService.insert(contrato3);
		
		Scanner input = new Scanner(System.in);
		String op;
		
		// Se le muestra al usuario una serie de opciones que puede elegir 
		// Al realizar una acción se vuelve a mostrar el menú hasta que decida cerrar la aplicación (eligiendo la última opción)
		do {
			System.out.println("--------------------------------------------------------------");
			System.out.println("Seleccione una opción");
			System.out.println("1. Listar Clientes");
			System.out.println("2. Listar Contratos");
			System.out.println("3. Listar Contratos Por Cliente");
			System.out.println("4. Búsqueda de Clientes Por Nombre y Apellidos");
			System.out.println("5. Añadir Nuevo Cliente");
			System.out.println("6. Asignar Contrato a Cliente");
			System.out.println("0. Salir");
			
			op = input.nextLine();
			String idSeleccionado;
			
			switch(op) {
				case "1":
					System.out.println("--1. Listar Clientes--");
					
					List<Cliente> listaClientes = clienteService.obtenerClientes();
					for (Cliente cliente : listaClientes) {
						System.out.println(cliente.toString());
					}
					
					System.out.println("");
					break;
				case "2":
					System.out.println("--2. Listar Contratos--");
					
					List<Contrato> listaContratos = contratoService.findAll();
					for (Contrato contrato : listaContratos) {
						System.out.println(contrato.toString());
					}
					
					System.out.println("");
					break;
				case "3":
					System.out.println("--3. Listar Contratos Por Cliente--");

					System.out.println("Introduzca el id del cliente");
					idSeleccionado = input.nextLine();
					
					if(idSeleccionado != null && idSeleccionado.length() > 0) {
						List<Contrato> contratosBusqueda = contratoService.findByIdCliente(Integer.parseInt(idSeleccionado));
						//contratosBusqueda = contratoService.criteriaFindByIdCliente(Integer.parseInt(idSeleccionado));
						for (Contrato contrato : contratosBusqueda) {
							System.out.println(contrato.toString());
						}

						if(contratosBusqueda.size() < 1)
							System.out.println("No se han encontrado contratos");
					}
					else {
						System.out.println("Id no válido");
					}
					
					System.out.println();
					break;
				case "4":
					System.out.println("--4. Búsqueda de Clientes Por Nombre y Apellidos--");

					System.out.println("Introduzca el nombre del cliente");
					String nombreInput = input.nextLine();
					System.out.println("Introduzca cualquiera de los apellidos del cliente");
					String apellidoInput = input.nextLine();
					
					List<Cliente> clientesBusqueda = clienteService.buscarPorNombreYApellido(nombreInput, apellidoInput);
					//clientesBusqueda = clienteService.criteriaFindByNameAndLastName(nombreInput, apellidoInput);
					for (Cliente cliente : clientesBusqueda) {
						System.out.println(cliente.toString());
					}
					

					if(clientesBusqueda.size() < 1)
						System.out.println("No se han encontrado clientes");
					
					System.out.println();
					break;
				case "5":
					System.out.println("--5. Añadir Nuevo Cliente--");

					Cliente nuevoCliente = new Cliente();
					
					System.out.println("Introduzca el dni del cliente");
					String dni = input.nextLine();
					nuevoCliente.setDocumentoIdentidad(dni);

					System.out.println("Introduzca el nombre del cliente");
					String nombre = input.nextLine();
					nuevoCliente.setNombre(nombre);

					System.out.println("Introduzca el primer apellido del cliente");
					String pApellido = input.nextLine();
					nuevoCliente.setPrimerApellido(pApellido);

					System.out.println("Introduzca el segundo apellido del cliente");
					String sApellido = input.nextLine();
					nuevoCliente.setSegundoApellido(sApellido);
					
					clienteService.agregarCliente(nuevoCliente);
					
					System.out.println();
					break;
				case "6":
					System.out.println("--6. Asignar Contrato a Cliente--");

					Contrato nuevoContrato = new Contrato();
					
					System.out.println("Introduzca el id del cliente");
					idSeleccionado = input.nextLine();
					nuevoContrato.setCliente(clienteService.buscarPorId(Integer.parseInt(idSeleccionado)));
					
					if(nuevoContrato.getCliente() != null) {
						System.out.println("Introduzca la fecha de caducidad (yyyy-MM-dd)");
						String fechaCad = input.nextLine();
						Date fechaCadDate = new SimpleDateFormat( "yyyy-MM-dd" ).parse(fechaCad);
						nuevoContrato.setFechaCaducidad(fechaCadDate);
						
						System.out.println("Introduzca el precio mensual");
						String precio = input.nextLine();
						nuevoContrato.setPrecioMensual(Double.parseDouble(precio));
						
						nuevoContrato.setFechaVigencia(new Date());
						
						contratoService.insert(nuevoContrato);
					
					} else {
						System.out.println("Cliente no encontrado");
					}
					
					System.out.println();
					break;
				case "0":
					System.out.println("Adiós");
					break;
				default:
					System.out.println("Opción no válida");
					break;
			}
			
		} while (!op.equalsIgnoreCase("0"));
		
		input.close();
		
	}

}
