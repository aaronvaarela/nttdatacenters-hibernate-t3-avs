package com.nttdata.services;

import java.util.List;

import com.nttdata.persistence.Cliente;

public interface ClienteServiceI {
	Cliente buscarPorId(int id);
	List<Cliente> buscarPorNombreYApellido(String nombre, String apellido);
	void borrarCliente(Cliente entity);
	Cliente actualizarCliente(Cliente entity);
	List<Cliente> obtenerClientes();
	void agregarCliente(Cliente entity);
	List<Cliente> criteriaFindByNameAndLastName(String name, String lastName);
}
