package com.nttdata.services;

import java.util.List;

import com.nttdata.persistence.Cliente;
import com.nttdata.persistence.Contrato;

public interface ContratoServiceI {

	Contrato findById(int id);
	List<Contrato> findAll();
	void insert(Contrato entity);
	Contrato update(Contrato entity);
	void delete(Contrato entity);
	
	List<Contrato> findByIdCliente(int idCliente);

	List<Contrato> criteriaFindByIdCliente(int id);
}
