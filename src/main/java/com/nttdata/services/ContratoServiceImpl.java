package com.nttdata.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nttdata.dao.ContratoDaoI;
import com.nttdata.persistence.Contrato;

@Service
public class ContratoServiceImpl implements ContratoServiceI {
	
	@Autowired
	private ContratoDaoI contratoDao;

	@Override
	public Contrato findById(int id) {
		return contratoDao.findById(id);
	}

	@Override
	public List<Contrato> findAll() {
		return contratoDao.findAll();
	}

	@Override
	public void insert(Contrato entity) {
		contratoDao.create(entity);
		
	}

	@Override
	public Contrato update(Contrato entity) {
		return contratoDao.update(entity);
	}

	@Override
	public void delete(Contrato entity) {
		contratoDao.delete(entity);
	}

	@Override
	public List<Contrato> findByIdCliente(int idCliente) {
		return contratoDao.findByIdCliente(idCliente);
	}

	@Override
	public List<Contrato> criteriaFindByIdCliente(int id) {
		return contratoDao.criteriaFindByIdCliente(id);
	}

}
