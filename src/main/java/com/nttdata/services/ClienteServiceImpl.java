package com.nttdata.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nttdata.dao.ClienteDaoImpl;
import com.nttdata.persistence.Cliente;
import java.util.Collections;

@Service
public class ClienteServiceImpl implements ClienteServiceI {

	@Autowired
	private ClienteDaoImpl clienteDao;
	
	@Override
	public Cliente buscarPorId(int id) {
		return clienteDao.findById(id);
	}

	@Override
	public List<Cliente> buscarPorNombreYApellido(String nombre, String apellido) {
		if(nombre != null && apellido != null)
			return clienteDao.findByNameAndLastName(nombre, apellido);
		else
			return Collections.emptyList();
	}

	@Override
	public void borrarCliente(Cliente entity) {
		clienteDao.delete(entity);
	}

	@Override
	public Cliente actualizarCliente(Cliente entity) {
		return clienteDao.update(entity);
	}

	@Override
	public List<Cliente> obtenerClientes() {
		return clienteDao.findAll();
	}

	@Override
	public void agregarCliente(Cliente entity) {
		clienteDao.create(entity);
	}

	@Override
	public List<Cliente> criteriaFindByNameAndLastName(String name, String lastName) {
		return clienteDao.criteriaFindByNameAndLastName(name, lastName);
	}

}
