package com.nttdata.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.nttdata.persistence.Cliente;

@Repository
public class ClienteDaoImpl extends CommonDaoImpl<Cliente> implements ClienteDaoI {

	/** Manejador de entidades */
	@Autowired
	private EntityManager entityManager;

	@Override
	@Transactional
	public List<Cliente> findByNameAndLastName(String name, String lastName) {
		// Obtención de sesión.
		Session currentSession = entityManager.unwrap(Session.class);
		
		List<Cliente> list = currentSession.createQuery("FROM Cliente WHERE nombre='" + name +
								"' AND (primerApellido='" + lastName + "' OR segundoApellido='" + lastName + "')").list();
		
		currentSession.close();
		
		return list;
	}
	
	@Transactional
	public List<Cliente> criteriaFindByNameAndLastName(String name, String lastName) {
		
		// Búsqueda de todos los registros
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<Cliente> cq = cb.createQuery(Cliente.class);
		Root<Cliente> root = cq.from(Cliente.class);
		
		Predicate pr1 = cb.equal(root.get("nombre"), name);
		Predicate pr2 = cb.equal(root.get("primerApellido"), lastName);
		Predicate pr3 = cb.equal(root.get("segundoApellido"), lastName);

		cq.select(root).where(cb.and(pr1, cb.or(pr2, pr3)));
		
		List<Cliente> results = entityManager.createQuery(cq).getResultList();

		return results;
	}

}
