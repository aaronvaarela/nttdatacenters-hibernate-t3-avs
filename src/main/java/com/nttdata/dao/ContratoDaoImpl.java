package com.nttdata.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.nttdata.persistence.Cliente;
import com.nttdata.persistence.Contrato;

@Repository
public class ContratoDaoImpl extends CommonDaoImpl<Contrato> implements ContratoDaoI {

	/** Manejador de entidades */
	@Autowired
	private EntityManager entityManager;

	@Override
	@Transactional
	public List<Contrato> findByIdCliente(int id) {

		// Obtención de sesión.
		Session currentSession = entityManager.unwrap(Session.class);
		
		List<Contrato> list = currentSession.createQuery("FROM Contrato WHERE cliente.id=" + id).list();
		
		currentSession.close();
		
		return list;
	}

	@Override
	public List<Contrato> criteriaFindByIdCliente(int id) {
		
		// Búsqueda de todos los registros
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<Contrato> cq = cb.createQuery(Contrato.class);
		Root<Contrato> root = cq.from(Contrato.class);
		Join<Contrato, Cliente> join = root.join("cliente");

		Predicate pr1 = cb.equal(join.get("id"), id);

		cq.select(root).where(pr1);
		
		List<Contrato> results = entityManager.createQuery(cq).getResultList();

		return results;
	}
	
	
	
}
