package com.nttdata.dao;

import java.util.List;

import com.nttdata.persistence.Contrato;

public interface ContratoDaoI extends CommonDaoI<Contrato> {
	List<Contrato> findByIdCliente(int id);
	List<Contrato> criteriaFindByIdCliente(int id);
}
