package com.nttdata.dao;

import java.util.List;

import com.nttdata.persistence.Cliente;

public interface ClienteDaoI extends CommonDaoI<Cliente> {
	
	List<Cliente> findByNameAndLastName(String name, String lastname);
	List<Cliente> criteriaFindByNameAndLastName(String name, String lastName);
	
}
