package com.nttdata.dao;

import java.lang.reflect.ParameterizedType;
import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.nttdata.persistence.AbstractEntity;

@Repository
public abstract class CommonDaoImpl<T extends AbstractEntity> implements CommonDaoI<T> {
	
	/** Tipo de clase */
	private Class<T> entityClass;

	/** Manejador de entidades */
	@Autowired
	private EntityManager entityManager;

	@SuppressWarnings("unchecked")
	public CommonDaoImpl() {
		setEntityClass((Class<T>) ((ParameterizedType) this.getClass().getGenericSuperclass()).getActualTypeArguments()[0]);
	}
	
	@Override
	@Transactional
	public T findById(int id) {

		// Obtención de sesión.
		Session currentSession = entityManager.unwrap(Session.class);

		// Búsqueda por id
		T result = currentSession.find(entityClass, id);

		// Cierre de sesión.
		currentSession.close();

		return result;
	}

	@Override
	@Transactional
	public List<T> findAll() {

		// Obtención de sesión.
		Session currentSession = entityManager.unwrap(Session.class);

		// Búsqueda de todos los registros.
		List<T> list = currentSession.createQuery("FROM " + this.entityClass.getName()).list();

		// Cierre de sesión.
		currentSession.close();
		
		//List<T> list2 = entityManager.createQuery("FROM " + this.entityClass.getName()).getResultList();
		
		return list;
	}

	@Override
	@Transactional
	public void create(T entity) {

		// Obtención de sesión.
		Session currentSession = entityManager.unwrap(Session.class);

		// Insercción.
		currentSession.save(entity);

		// Cierre de sesión.
		currentSession.close();
		
		//entityManager.persist(entity);
		
	}

	@Override
	@Transactional
	public T update(T entity) {

		// Obtención de sesión.
		Session currentSession = entityManager.unwrap(Session.class);

		// Update.
		currentSession.saveOrUpdate(entity);

		// Cierre de sesión.
		currentSession.close();
		
		return entity;
	}

	@Override
	@Transactional
	public void delete(T entity) {

		// Obtención de sesión.
		Session currentSession = entityManager.unwrap(Session.class);

		// Delete.
		currentSession.delete(entity);

		// Cierre de sesión.
		currentSession.close();
		
	}

	public void setEntityClass(Class<T> entityClass) {
		this.entityClass = entityClass;
	}
	
	

}
