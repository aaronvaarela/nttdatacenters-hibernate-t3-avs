package com.nttdata.persistence;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Digits;

@Entity
@Table(name="Contrato")
public class Contrato extends AbstractEntity {

	@Id
	@GeneratedValue
	@Column(name="id_contract")
	private int id;

	@Column
	@Temporal(TemporalType.DATE)
	private Date fechaVigencia;
	
	@Column
	@Temporal(TemporalType.DATE)
	private Date fechaCaducidad;
	
	@Column
	@Digits(fraction = 2, integer = 5)
	private Double precioMensual;
	
	@ManyToOne
	@JoinColumn(name = "cliente_id")
	private Cliente cliente;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getFechaVigencia() {
		return fechaVigencia;
	}

	public void setFechaVigencia(Date fechaVigencia) {
		this.fechaVigencia = fechaVigencia;
	}

	public Date getFechaCaducidad() {
		return fechaCaducidad;
	}

	public void setFechaCaducidad(Date fechaCaducidad) {
		this.fechaCaducidad = fechaCaducidad;
	}

	public Double getPrecioMensual() {
		return precioMensual;
	}

	public void setPrecioMensual(Double precioMensual) {
		this.precioMensual = precioMensual;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	@Override
	public String toString() {
		return "Contrato [id=" + id + ", fechaVigencia=" + fechaVigencia + ", fechaCaducidad=" + fechaCaducidad
				+ ", precioMensual=" + precioMensual + ", cliente=" + cliente + "]";
	}
	
	
	
}
