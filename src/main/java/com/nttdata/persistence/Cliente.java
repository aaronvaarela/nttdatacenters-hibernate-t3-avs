package com.nttdata.persistence;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="Client")
public class Cliente extends AbstractEntity {
	
	@Id
	@GeneratedValue
	@Column(name="id_player")
	private int id;
	
	@Column(name="name")
	private String nombre;
	
	@Column(name="first_last_name")
	private String primerApellido;
	
	@Column(name="second_last_name")
	private String segundoApellido;
	
	@Column(name="document_id", unique=true, nullable=false, length = 9)
	private String documentoIdentidad;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "cliente")
	private List<Contrato> listaContratos;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getPrimerApellido() {
		return primerApellido;
	}

	public void setPrimerApellido(String primerApellido) {
		this.primerApellido = primerApellido;
	}

	public String getSegundoApellido() {
		return segundoApellido;
	}

	public void setSegundoApellido(String segundoApellido) {
		this.segundoApellido = segundoApellido;
	}

	public String getDocumentoIdentidad() {
		return documentoIdentidad;
	}

	public void setDocumentoIdentidad(String documentoIdentidad) {
		this.documentoIdentidad = documentoIdentidad;
	}

	@Override
	public String toString() {
		return "Cliente [id=" + id + ", nombre=" + nombre + ", primerApellido=" + primerApellido + ", segundoApellido="
				+ segundoApellido + ", documentoIdentidad=" + documentoIdentidad + "]";
	}
	
}
